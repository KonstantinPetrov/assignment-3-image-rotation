#ifndef IMAGE_TRANSFORMER_TRANSFORMATE_H
#define IMAGE_TRANSFORMER_TRANSFORMATE_H


#include "stdint.h"
#include <stdlib.h>


enum turn_result{
    TURN_OK=0,
    TURN_ERROR=1
};


enum turn_result transform_image(const char* input_image, const char* output_image);

#endif //IMAGE_TRANSFORMER_TRANSFORMATE_H
