#ifndef IMAGE_TRANSFORMER_FILE_CLOSER_H
#define IMAGE_TRANSFORMER_FILE_CLOSER_H

#include <stdio.h>

enum close_status {
    CLOSE_OK = 0,
    CLOSE_ERROR=1
};

enum close_status file_close(FILE *file);

#endif //IMAGE_TRANSFORMER_FILE_CLOSER_H
