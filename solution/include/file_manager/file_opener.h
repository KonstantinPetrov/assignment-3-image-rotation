#ifndef IMAGE_TRANSFORMER_FILE_OPENER_H
#define IMAGE_TRANSFORMER_FILE_OPENER_H

#include <stdio.h>

enum open_status {
    OPEN_OK = 0,
    OPEN_ERROR=1
};
enum open_status file_open(FILE **file, const char *image_name, const char *mode);
#endif //IMAGE_TRANSFORMER_FILE_OPENER_H
