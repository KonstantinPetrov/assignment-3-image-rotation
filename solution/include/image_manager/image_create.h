#ifndef IMAGE_TRANSFORMER_IMAGE_CREATE_H
#define IMAGE_TRANSFORMER_IMAGE_CREATE_H

#include <stdint.h>


struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct __attribute__((packed)) pixel
{
    uint8_t b, g, r;
};

struct image allocate_image_memory(uint64_t wight, uint64_t height);

#endif //IMAGE_TRANSFORMER_IMAGE_CREATE_H
