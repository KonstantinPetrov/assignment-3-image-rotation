#ifndef IMAGE_TRANSFORMER_ROTATE_BMP_H
#define IMAGE_TRANSFORMER_ROTATE_BMP_H

/* создаёт копию изображения, которая повёрнута на 90 градусов */
struct image rotate( struct image const source );


#endif //IMAGE_TRANSFORMER_ROTATE_BMP_H
