#include "file_manager/file_opener.h"


enum open_status file_open(FILE **file, const char *image_name, const char *mode) {
    *file = fopen(image_name, mode);
    if (*file) {
        return OPEN_OK;
    }else{
        printf("Error open file");
        return OPEN_ERROR;
    }
}
