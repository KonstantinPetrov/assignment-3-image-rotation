#include "file_manager/file_closer.h"

enum close_status file_close(FILE *file) {
    int result_close = fclose(file);
    if (result_close!=0){
        return CLOSE_ERROR;
    } else{
        return CLOSE_OK;
    }
}
