#include "image_manager/image_create.h"
#include <stdlib.h>

struct image rotate(struct image const source) {
    struct image turn_image = allocate_image_memory(source.height, source.width);

    for(size_t i=0;i< source.height;i++){
        for(size_t j=0;j<source.width;j++){
            turn_image.data[j * source.height + source.height - i - 1] = source.data[i * source.width + j];
        }
    }

    return turn_image;

}
