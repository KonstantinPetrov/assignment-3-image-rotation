#include "image_manager/image_create.h"

#include "malloc.h"

struct image allocate_image_memory(uint64_t wight, uint64_t height) {
    struct image image = {.width=wight, .height=height, .data=malloc(wight * height * sizeof(struct pixel))};
    return image;
}
