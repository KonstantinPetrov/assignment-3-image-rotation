#include "image_manager/bmp_worker.h"

enum data_bmpheader {
    DFTYPE = 19778,
    DFRESERVED = 0,
    BOFFBITS = 54,
    BISIZE = 40,
    BIPLANES = 1,
    BIBITCOUNT = 24,
    BICOMPRESSION = 0,
    BIXPELSPERMETER = 2834,
    BIYPELSPERMETER = 2834,
    BICLRUSED = 0,
    BICLRIMPORTANT = 0
};

enum read_status from_bmp(FILE *in, struct image *img) {
    struct bmp_header bmp_header={0};


    if (fread(&bmp_header, sizeof(struct bmp_header), 1, in) != 1) {
        printf("read invalid header\n");
        return READ_INVALID_HEADER;
    }

    *img = allocate_image_memory(bmp_header.biWidth, bmp_header.biHeight);

    int padding = (4 - (int) (bmp_header.biWidth * sizeof(struct pixel)) % 4) % 4;


    fseek(in, bmp_header.bOffBits, SEEK_SET);

    for (size_t i = 0; i < img->height; i++) {
        if (fread(img->data + img->width * i, sizeof(struct pixel), img->width, in) != img->width) {
            printf("read invalid bits\n");
            return READ_INVALID_BITS;
        } else {
            fseek(in, padding, SEEK_CUR);
        }
    }

    return READ_OK;


}

enum write_status to_bmp(FILE *out, struct image const *img) {
    int padding = (4 - (int) (img->width * sizeof(struct pixel)) % 4) % 4;
    struct bmp_header bmp_header = {
            .bfType=DFTYPE,
            .bfileSize=img->height * padding + BOFFBITS + sizeof(struct pixel) * img->height * img->width,
            .bfReserved=DFRESERVED,
            .bOffBits=BOFFBITS,
            .biSize=BISIZE,
            .biHeight=img->height,
            .biWidth=img->width,
            .biPlanes=BIPLANES,
            .biBitCount=BIBITCOUNT,
            .biCompression=BICOMPRESSION,
            .biSizeImage=img->height * img->width,
            .biXPelsPerMeter=BIXPELSPERMETER,
            .biYPelsPerMeter=BIYPELSPERMETER,
            .biClrUsed=BICLRUSED,
            .biClrImportant=BICLRIMPORTANT
    };

    if (fwrite(&bmp_header, sizeof(struct bmp_header), 1, out) != 1) {
        return WRITE_ERROR;
    }

    for (size_t i = 0; i < img->height; i++) {
        if (fwrite(img->data + i * img->width, sizeof(struct pixel), img->width, out) != img->width) {
            return WRITE_ERROR;
        }

        if (fseek(out, padding, SEEK_CUR) != 0) {
            return WRITE_ERROR;
        }
    }
    return WRITE_OK;
}
