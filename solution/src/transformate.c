#include "transformate.h"
#include "file_manager/file_closer.h"
#include "file_manager/file_opener.h"
#include "image_manager/bmp_worker.h"
#include "image_manager/image_create.h"
#include "image_manager/rotate_bmp.h"
#include "malloc.h"



enum turn_result transform_image(const char* input_image, const char* output_image){
    FILE *old_file,*new_file;

    if(file_open(&old_file, input_image, "rb")==OPEN_OK && file_open(&new_file, output_image, "wb")==OPEN_OK){

        struct image image={0};

        enum read_status result_reading = from_bmp(old_file, &image);

        if (result_reading!=READ_OK) {
            file_close(old_file);
            file_close(new_file);
            free(image.data);

            printf("Error in reading image");
            return TURN_ERROR;
        }

        struct image turn_image = rotate(image);
        free(image.data);

        enum write_status result_write=to_bmp(new_file, &turn_image);

        if(result_write!=WRITE_OK){
            file_close(old_file);
            file_close(new_file);

            printf("Error in writing image");
            return TURN_ERROR;
        }


        free(turn_image.data);
        fclose(old_file);
        fclose(new_file);

        return TURN_OK;
    }else{
        printf("Failed to open file");
        return TURN_ERROR;
    }

}
