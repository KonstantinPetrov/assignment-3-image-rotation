#include "transformate.h"

#include <stdio.h>

int main(int argc, char **argv) {

    if(argc == 3){
        return transform_image(argv[1], argv[2]);
    }else{
        printf("Incorrect input!");
        return 1;
    }
}
